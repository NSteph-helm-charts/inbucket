# Inbucket

A Helm chart for deploying Inbucket on Kubernetes


## Prerequisites

- Kubernetes 1.19+


## Installing the Chart
```console
$ REPO_URL=registry.gitlab.com/nsteph-helm-charts/inbucket/inbucket
$ VERSION=0.1.0.3-SNAPSHOT

$ export HELM_EXPERIMENTAL_OCI=1
$ helm chart pull $REPO_URL:$VERSION
$ helm chart export $REPO_URL:$VERSION -d ./

$ helm install my-release ./inbucket
```

## Uninstalling the Chart

To uninstall/delete the `my-release` deployment:

```console
$ helm delete my-release
```
## Configuration

The following table lists the configurable parameters of the inbucket chart and their default values.

| Parameter | Description | Default |
| --------- | ----------- | ------- |
| `global.imagePullSecrets` | Reference to one or more secrets to be used when pulling images | `[]` |
| `image.repository` | Image repository | `inbucket/inbucket` |
| `image.tag` | Image tag | `stable` |
| `image.pullPolicy` | Image pull policy | `IfNotPresent` |
| `replicaCount`  | Number of  replicas | `1` |
| `serviceAccount.create` | If `true`, create a new service account | `false` |
| `serviceAccount.name` | Service account to be used. If not set and `serviceAccount.create` is `true`, a name is generated using the fullname template |  |
| `service.port.smtp` | Service port number to expose the SMTP | `25` |
| `service.port.pop3` | Service port number to expose the POP3 | `110` |
| `service.port.http` | Service port number to expose the Web interface | `80` |
| `service.nodePort.enabled` | Expose the SMTP as a nodePort | `false` |
| `service.nodePort.smtp` | Service port number to expose the SMTP as a nodePort | `32500` |
| `ingress.enabled` | Enable Ingress to the Inbucket webinterface | `false` |
| `ingress.annotations` | Add annotations to the Ingress | `{}` |
| `ingress.hosts` | List of hosts to map to the Ingress | `[...]` |
| `ingress.tls` | secretName to use for the Ingress' TLS | `[]` |
| `resources` | CPU/memory resource requests/limits | |
| `resources.limits.cpu` | cpu resource limits | `100m` |
| `resources.limits.memory` | memory resource limits | `64Mi` |
| `securityContext.enabled` | Enable security context | `false` |
| `securityContext.fsGroup` | Group ID for the container | `1001` |
| `securityContext.runAsUser` | User ID for the container | `1001` |
| `persistence.enabled` | Enable persistence in the form of a PV | `false` |
| `persistence.mountPath` | Mount path of the PV to the container | `/storage` |
| `persistence.storageSize` | Size of the PV | `1Gi` |
| `nodeSelector` | Node labels for pod assignment | `{}` |
| `affinity` | Node affinity for pod assignment | `{}` |
| `tolerations` | Node tolerations for pod assignment | `[]` |
| `podAnnotations` | Annotations to add to the inbucket pod | `{}` |

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`.

## Contributing

This chart is maintained at [gitlab.com/NSteph-helm-charts/inbucket](https://gitlab.com/NSteph-helm-charts/inbucket). \
Issues and PRs are welcomed.